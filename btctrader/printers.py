import datetime, decimal, monotable.table


def format_dict_list(data, headers, *, sort_key=None, reverse=True, convert_date=True):
    if sort_key is not None:
        data = sorted(data, key=lambda k:k[sort_key], reverse=reverse)
    data_list = []
    for d in data:
        row = []
        for h in headers:
            if h == "date":
                if convert_date:
                    row.append(datetime.datetime.fromtimestamp(d[h]))
                else:
                    row.append(d[h])
            elif h == "cpair":
                row.append("/".join(d[h]))
            else:
                row.append(d[h])
        data_list.append(row)
    formats = []
    if data_list:
        for v in data_list[0]:
            if isinstance(v, decimal.Decimal):
                formats.append("f")
            else:
                formats.append("")
    else:
        data_list = [[None] * len(headers)]
    return monotable.table.table(headings=headers, cellgrid=data_list, formats=formats)

def public_transactions(data):
    return format_dict_list(
        data,
        ["tid", "date", "price", "amount", "type", "cpair"],
        sort_key="tid")

def public_offers(data):
    return format_dict_list(
        data["bids"] + data["asks"],
        ["price", "amount", "type", "cpair"],
        sort_key="price")

def public_ticker(data):
    return format_dict_list(
        [data],
        ["bid", "ask", "high", "low", "last", "volume", "cpair"])

def private_transactions(data):
    return format_dict_list(
        data,
        ["tid", "date", "price", "amount", "type", "cpair"],
        sort_key="tid")

def private_funds(data):
    return format_dict_list(
        data,
        ["currency", "total", "blocked", "available"])

def private_offers(data):
    return format_dict_list(
        data["bids"] + data["asks"],
        ["oid", "price", "amount", "type", "cpair"],
        sort_key="oid")
