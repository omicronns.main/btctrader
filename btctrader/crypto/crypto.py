import ecdsa


def legendre_symbol(a, p):
    """ Compute the Legendre symbol a|p using
        Euler's criterion. p is a prime, a is
        relatively prime to p (if p divides
        a, then a|p = 0)

        Returns 1 if a has a square root modulo
        p, -1 otherwise.
    """
    ls = pow(a, (p - 1) // 2, p)
    return -1 if ls == p - 1 else ls


def modular_sqrt(a, p):
    """ Find a quadratic residue (mod p) of 'a'. p
        must be an odd prime.

        Solve the congruence of the form:
            x^2 = a (mod p)
        And returns x. Note that p - x is also a root.

        0 is returned is no square root exists for
        these a and p.

        The Tonelli-Shanks algorithm is used (except
        for some simple cases in which the solution
        is known from an identity). This algorithm
        runs in polynomial time (unless the
        generalized Riemann hypothesis is false).
    """
    # Simple cases
    #
    if legendre_symbol(a, p) != 1:
        return 0
    elif a == 0:
        return 0
    elif p == 2:
        return p
    elif p % 4 == 3:
        return pow(a, (p + 1) // 4, p)

    # Partition p-1 to s * 2^e for an odd s (i.e.
    # reduce all the powers of 2 from p-1)
    #
    s = p - 1
    e = 0
    while s % 2 == 0:
        s //= 2
        e += 1

    # Find some 'n' with a legendre symbol n|p = -1.
    # Shouldn't take long.
    #
    n = 2
    while legendre_symbol(n, p) != -1:
        n += 1

    # Here be dragons!
    # Read the paper "Square roots from 1; 24, 51,
    # 10 to Dan Shanks" by Ezra Brown for more
    # information
    #

    # x is a guess of the square root that gets better
    # with each iteration.
    # b is the "fudge factor" - by how much we're off
    # with the guess. The invariant x^2 = ab (mod p)
    # is maintained throughout the loop.
    # g is used for successive powers of n to update
    # both a and b
    # r is the exponent - decreases with each update
    #
    x = pow(a, (s + 1) // 2, p)
    b = pow(a, s, p)
    g = pow(n, s, p)
    r = e

    while True:
        t = b
        m = 0
        for m in range(r):
            if t == 1:
                break
            t = pow(t, 2, p)

        if m == 0:
            return x

        gs = pow(g, 2 ** (r - m - 1), p)
        g = (gs * gs) % p
        x = (x * gs) % p
        b = (b * g) % p
        r = m


def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)


def modular_inv(a, m):
    g, x, y = egcd(a, m)
    if g == 1:
        return x % m


def compute_y_from_x(x, m = None):
    a = ecdsa.SECP256k1.curve.a()
    b = ecdsa.SECP256k1.curve.b()
    p = ecdsa.SECP256k1.curve.p()
    y2 = (pow(x, 3, p) + a * x + b) % p
    yq = modular_sqrt(y2, p)
    y = [yq, p - yq]
    if yq == 0:
        return
    if m is None:
        return y
    elif m == 2:
        if y[0] % 2 == 0:
            resy = y[0]
        if y[1] % 2 == 0:
            resy = y[1]
    elif m == 3:
        if y[0] % 2 != 0:
            resy = y[0]
        if y[1] % 2 != 0:
            resy = y[1]
    return resy


def compute_x_from_r(r, n, recid):
    return (r + (n * (recid // 2)))


def compute_pub_key_from_signature(signature, data_hash, flag):
    try:
        h = (int).from_bytes(data_hash, "big")
        r = (int).from_bytes(signature[:32], "big")
        s = (int).from_bytes(signature[32:], "big")
        x = compute_x_from_r(r, ecdsa.SECP256k1.generator.order(), flag - 27)
        if flag % 2 == 1:
            y = compute_y_from_x(x, 2)
        else:
            y = compute_y_from_x(x, 3)
        if y is None:
            return
        R = ecdsa.ellipticcurve.Point(ecdsa.SECP256k1.curve, x, y)
        eG = h * ecdsa.SECP256k1.generator
        eG = ecdsa.ellipticcurve.Point(ecdsa.SECP256k1.curve, eG.x(), (-eG.y()) % ecdsa.SECP256k1.curve.p())
        SR = s * R
        pk = modular_inv(r, ecdsa.SECP256k1.generator.order()) * (SR + eG)
        return pk
    except:
        return None


def compute_signature_flag(signature, pub_key, data_hash):
    x = (int).from_bytes(pub_key[:32], "big")
    y = (int).from_bytes(pub_key[32:], "big")
    for flag in range(27, 31):
        pk = compute_pub_key_from_signature(signature, data_hash, flag)
        if pk is None:
            continue
        if x == pk.x() and y == pk.y():
            return flag.to_bytes(1, "big")
