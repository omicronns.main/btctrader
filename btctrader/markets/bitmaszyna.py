import decimal, base64, requests, urllib, hashlib, time, ecdsa
import btctrader.markets.market as market
import btctrader.crypto.crypto as crypto


class public(market.public):
    def get_transactions(self, cpair):
        transactions = self.__public_request(cpair, "transactions")
        transactions_list = []
        for record in transactions:
            type_map = {1: "bid", 2: "ask"}
            transactions_list.append({
                "tid": record["tid"],
                "date": record["date"],
                "amount": record["amount"],
                "price": record["price"],
                "type": type_map[record["type"]],
                "cpair": cpair
            })
        return transactions_list

    def get_offers(self, cpair):
        offers = self.__public_request(cpair, "depth")["offers" + "".join(cpair)]
        bids = offers["bids"]
        asks = offers["asks"]
        bids_list = []
        for record in bids:
            bids_list.append({
                "amount": record[1],
                "price": record[2],
                "type": "bid",
                "cpair": cpair
            })
        asks_list = []
        for record in asks:
            asks_list.append({
                "amount": record[1],
                "price": record[2],
                "type": "ask",
                "cpair": cpair
            })
        return {"bids": bids_list, "asks": asks_list}

    def get_ticker(self, cpair):
        ticker = self.__public_request(cpair, "ticker")
        return {
            "bid": ticker["bid"],
            "ask": ticker["ask"],
            "high": ticker["high"],
            "low": ticker["low"],
            "last": ticker["last"],
            "volume": ticker["volume1"],
            "cpair": cpair
        }

    def __public_request(self, cpair, reqtype):
        resp = requests.get("https://bitmaszyna.pl/api/%s/%s.json" % ("".join(cpair), reqtype))
        return resp.json(parse_float=decimal.Decimal)


class private(market.private):
    def currencies(self):
        return ["PLN", "BTC", "LTC", "DOGE", "LSK"]

    def setup(self, credentials):
        self.prv_key = ecdsa.SigningKey.from_string(base64.b64decode(credentials["prv_key"]), ecdsa.SECP256k1)
        self.api_key = credentials["api_key"].encode("utf-8")

    def get_funds(self):
        funds = self.__private_request("funds")["funds"]
        funds_list = []
        for currency in self.currencies():
            funds_list.append({
                "currency": currency,
                "total": funds["total_" + currency],
                "blocked": funds["blocked_" + currency],
                "available": funds["available_" + currency]
            })
        return funds_list

    def get_transactions(self):
        transactions = self.__private_request("history")["ahistory"]
        transactions_list = []
        for record in transactions:
            type_map = {"Buy": "bid", "Sell": "ask"}
            if record["type"] in type_map.keys():
                transaction = {
                    "tid": record["tid"],
                    "date": record["date"],
                    "cpair": tuple(record["pair"].split("/")),
                    "price": record["price"],
                    "amount": record["amount"],
                    "type": type_map[record["type"]]
                }
                if transactions_list and transactions_list[-1]["tid"] == record["tid"]:
                    if transaction["type"] == "bid" and transaction["amount"] > 0:
                        transactions_list[-1] = transaction
                    elif transaction["type"] == "ask" and transaction["amount"] > 0:
                        transactions_list[-1]["amount"] = abs(transactions_list[-1]["amount"])
                    elif transaction["type"] == "ask" and transaction["amount"] < 0:
                        transaction["amount"] = abs(transaction["amount"])
                        transactions_list[-1] = transaction
                else:
                    transactions_list.append(transaction)
        return transactions_list

    def get_offers(self):
        offers = self.__private_request("offers")["orders"]
        offers_list = []
        for record in offers:
            type_map = {0: "bid", 1: "ask"}
            offers_list.append({
                "oid": record["id"],
                "cpair": tuple(record["market"].split("/")),
                "price": record["price"],
                "amount": record["amount"],
                "type": type_map[record["type"]]
            })
        return offers_list

    def make_bid(self, cpair, price, amount):
        offer = self.__private_request(
            urllib.parse.urljoin("".join(cpair), "buy"), {
                "amount": amount,
                "price": price,
                "offer": "true"
            })
        return offer

    def make_ask(self, cpair, price, amount):
        offer = self.__private_request(
            urllib.parse.urljoin("".join(cpair), "sell"), {
                "amount": amount,
                "price": price,
                "offer": "false"
            })
        return offer

    def cancel(self, oid):
        raise NotImplementedError

    def __private_request(self, reqtype, params={}):
        params["nonce"] = int(time.time())
        params_data = ("Bitmaszyna.pl API:\n" + urllib.parse.urlencode(params)).encode("utf-8")
        params_hash = hashlib.sha256(hashlib.sha256(params_data).digest()).digest()
        pub_key_bytes = self.prv_key.get_verifying_key().to_string()
        signature_flag = None
        while signature_flag is None:
            signature = self.prv_key.sign_digest(params_hash)
            signature_flag = crypto.compute_signature_flag(signature, pub_key_bytes, params_hash)
        headers = {"Rest-Key": self.api_key, "Rest-Sign": base64.b64encode(signature_flag + signature)}
        resp = requests.post("https://bitmaszyna.pl/api/%s" % reqtype, headers=headers, data=params)
        return resp.json(parse_float=decimal.Decimal)
