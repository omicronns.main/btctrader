import decimal, base64, requests, urllib, hashlib, time, datetime, hmac
import btctrader.markets.market as market
import btctrader.crypto.crypto as crypto


def datestr2timestamp(datestr):
    return int(datetime.datetime.strptime(datestr, "%Y-%m-%dT%H:%M:%SZ").timestamp())


class public(market.public):
    def get_transactions(self, cpair):
        transactions = self.__public_request(cpair, "trades")
        if not isinstance(transactions, list):
            return None
        transactions_list = []
        for record in transactions:
            type_map = {"buy": "bid", "sell": "ask"}
            transactions_list.append({
                "tid": record["trade_id"],
                "date": datestr2timestamp(record["time"]),
                "amount": decimal.Decimal(record["size"]),
                "price": decimal.Decimal(record["price"]),
                "type": type_map[record["side"]],
                "cpair": cpair
            })
        return transactions_list

    def get_offers(self, cpair):
        offers = self.__public_request(cpair, "book")
        bids = offers["bids"]
        asks = offers["asks"]
        if not bids and not asks:
            return None
        bids_list = []
        for record in bids:
            bids_list.append({
                "amount": decimal.Decimal(record[1]),
                "price": decimal.Decimal(record[0]),
                "type": "bid",
                "cpair": cpair
            })
        asks_list = []
        for record in asks:
            asks_list.append({
                "amount": decimal.Decimal(record[1]),
                "price": decimal.Decimal(record[0]),
                "type": "ask",
                "cpair": cpair
            })
        return {"bids": bids_list, "asks": asks_list}

    def get_ticker(self, cpair):
        ticker = self.__public_request(cpair, "ticker")
        stats = self.__public_request(cpair, "stats")
        if "message" in ticker.keys():
            return None
        return {
            "bid": decimal.Decimal(ticker["bid"]),
            "ask": decimal.Decimal(ticker["ask"]),
            "high": decimal.Decimal(stats["high"]),
            "low": decimal.Decimal(stats["low"]),
            "last": decimal.Decimal(ticker["price"]),
            "volume": decimal.Decimal(ticker["volume"]),
            "cpair": cpair
        }

    def __public_request(self, cpair, reqtype):
        resp = requests.get("https://api.abucoins.com/products/%s/%s" % ("-".join(cpair), reqtype))
        return resp.json(parse_float=decimal.Decimal)


class auth(requests.auth.AuthBase):
    def __init__(self, key, secret, passphrase):
        self.key = key
        self.secret = base64.b64decode(secret)
        self.passphrase = passphrase

    def __call__(self, request):
        timestamp = str(int(time.time())) # eg. 1512516837
        # timestamp = eg. 1512516837
        # request.method = eg. POST
        # request.path_url = eg. /orders
        message = timestamp + request.method + request.path_url
        if request.body: # if present
            message = message + request.body.decode() # decode raw bytes

        hmac_key = self.secret
        signature = hmac.new(hmac_key, message.encode('utf-8'), hashlib.sha256)
        signature_base64 = base64.b64encode(signature.digest())

        request.headers.update({
            'AC-ACCESS-KEY': self.key,
            'AC-ACCESS-PASSPHRASE': self.passphrase,
            'AC-ACCESS-SIGN': signature_base64,
            'AC-ACCESS-TIMESTAMP' : timestamp,
        })
        return request


class private(market.private):
    def setup(self, credentials):
        self.authenticator = auth(
            credentials["key"].encode("utf-8"),
            credentials["secret"].encode("utf-8"),
            credentials["passphrase"].encode("utf-8"))
        self.api_url = "https://api.abucoins.com"

    def get_funds(self):
        req_url = urllib.parse.urljoin(self.api_url, "accounts")
        funds = requests.get(req_url, auth=self.authenticator).json(parse_float=decimal.Decimal)
        funds_list = []
        for record in funds:
            funds_list.append({
                "currency": record["currency"],
                "total": decimal.Decimal(record["balance"]),
                "blocked": decimal.Decimal(record["hold"]),
                "available": decimal.Decimal(record["available"])
            })
        return funds_list

    def get_transactions(self):
        req_url = urllib.parse.urljoin(self.api_url, "fills")
        transactions = requests.get(req_url, auth=self.authenticator).json(parse_float=decimal.Decimal)
        transactions_list = []
        for record in transactions:
            type_map = {"buy": "bid", "sell": "ask"}
            if record["side"] in type_map.keys():
                transaction = {
                    "tid": record["trade_id"],
                    "date": datestr2timestamp(record["created_at"]),
                    "cpair": tuple(record["product_id"].split("-")),
                    "price": decimal.Decimal(record["price"]),
                    "amount": decimal.Decimal(record["size"]),
                    "type": type_map[record["side"]]
                }
                transactions_list.append(transaction)
        return transactions_list

    def get_offers(self):
        req_url = urllib.parse.urljoin(self.api_url, "orders")
        offers = requests.get(req_url, auth=self.authenticator).json(parse_float=decimal.Decimal)
        bids_list = []
        asks_list = []
        for record in offers:
            type_map = {"buy": "bid", "sell": "ask"}
            offer = {
                "oid": record["id"],
                "cpair": tuple(record["product_id"].split("-")),
                "price": decimal.Decimal(record["price"]),
                "amount": decimal.Decimal(record["size"]),
                "type": type_map[record["side"]]
            }
            if offer["type"] == "bid":
                bids_list.append(offer)
            if offer["type"] == "ask":
                asks_list.append(offer)
        return {"bids": bids_list, "asks": asks_list}

    def make_bid(self, cpair, price, amount):
        req_url = urllib.parse.urljoin(self.api_url, "orders")
        offer = requests.post(req_url, auth=self.authenticator, json={
            "product_id": "-".join(cpair),
            "price": format(price, "f"),
            "size": format(amount, "f"),
            "side": "buy",
            "type": "limit"
        }).json(parse_float=decimal.Decimal)
        return int(offer["id"])

    def make_ask(self, cpair, price, amount):
        req_url = urllib.parse.urljoin(self.api_url, "orders")
        offer = requests.post(req_url, auth=self.authenticator, json={
            "product_id": "-".join(cpair),
            "price": format(price, "f"),
            "size": format(amount, "f"),
            "side": "sell",
            "type": "limit"
        }).json(parse_float=decimal.Decimal)
        return int(offer["id"])

    def cancel(self, oid):
        req_url = urllib.parse.urljoin(self.api_url, "orders/%s" % oid)
        cancel = requests.delete(req_url, auth=self.authenticator).json(parse_float=decimal.Decimal)
        if len(cancel) == 1:
            return cancel[0] == oid
        else:
            return False
