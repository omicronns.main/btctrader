import abc


class public(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_transactions(self, cpair):
        raise NotImplementedError

    @abc.abstractmethod
    def get_offers(self, cpair):
        raise NotImplementedError

    @abc.abstractmethod
    def get_ticker(self, cpair):
        raise NotImplementedError


class private(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_funds(self):
        raise NotImplementedError

    @abc.abstractmethod
    def get_transactions(self):
        raise NotImplementedError

    @abc.abstractmethod
    def get_offers(self):
        raise NotImplementedError

    @abc.abstractmethod
    def make_bid(self, cpair, price, *, amount=None, amount_quota=None):
        raise NotImplementedError

    @abc.abstractmethod
    def make_ask(self, cpair, price, *, amount=None, amount_quota=None):
        raise NotImplementedError

    @abc.abstractmethod
    def cancel(self, oid):
        raise NotImplementedError
