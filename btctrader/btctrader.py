import click, importlib, json, decimal
import btctrader.printers as printers


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return "{:f}".format(o)
        return super(DecimalEncoder, self).default(o)


def create_public(market_name):
    market_module = importlib.import_module("btctrader.markets.%s" % market_name)
    market_class = getattr(market_module, "public")
    return market_class()


def create_private(market_name):
    market_module = importlib.import_module("btctrader.markets.%s" % market_name)
    market_class = getattr(market_module, "private")
    return market_class()


def filter_last(data, time_back):
    date_threshold = data["date"] - time_back
    return filter(lambda x: x["date"] > date_threshold, data)


def split_cpair(ctx, param, value):
    return value.split("/")


@click.group()
def btctrader():
    pass


@btctrader.group()
def public():
    pass


@btctrader.group()
def private():
    pass


@public.command()
@click.argument("market_name")
@click.argument("cpair", callback=split_cpair)
@click.option("--indent", type=int, help="format using INDENT indentation")
@click.option("--raw", is_flag=True, help="display raw response")
@click.option("--count", type=click.IntRange(min=0), default=0, help="display only COUNT items")
@click.option("--age", type=click.IntRange(min=0), default=0, help="display only AGE old entries, in unix timestamp ticks")
def transactions(market_name, cpair, indent, raw, count, age):
    public_api = create_public(market_name)
    data = public_api.get_transactions(cpair)
    if data is None:
        raise RuntimeError("could not fetch data")
    if age > 0 and data:
        data = filter_last(data[0]["date"] - age, data)
    if count > 0 and count <= len(data):
        data = data[:count]
    if raw:
        print(json.dumps(data, indent=indent, cls=DecimalEncoder))
    else:
        print(printers.public_transactions(data))


@public.command()
@click.argument("market_name")
@click.argument("cpair", callback=split_cpair)
@click.option("--indent", type=int, help="format using INDENT indentation")
@click.option("--raw", is_flag=True, help="display raw response")
@click.option("--count", type=int, default=0, help="display only COUNT items (COUNT bids and COUNT asks)")
def offers(market_name, cpair, indent, raw, count):
    public_api = create_public(market_name)
    data = public_api.get_offers(cpair)
    if data is None:
        raise RuntimeError("could not fetch data")
    if count > 0 and count <= len(data["bids"]) and count <= len(data["asks"]):
        data = {"bids": data["bids"][:count], "asks": data["asks"][:count]}
    if raw:
        print(json.dumps(data, indent=indent, cls=DecimalEncoder))
    else:
        print(printers.public_offers(data))


@public.command()
@click.argument("market_name")
@click.argument("cpair", callback=split_cpair)
@click.option("--indent", type=int, help="format using INDENT indentation")
@click.option("--raw", is_flag=True, help="display raw response")
def ticker(market_name, cpair, indent, raw):
    public_api = create_public(market_name)
    data = public_api.get_ticker(cpair)
    if data is None:
        raise RuntimeError("could not fetch data")
    if raw:
        print(json.dumps(data, indent=indent, cls=DecimalEncoder))
    else:
        print(printers.public_ticker(data))


@private.command()
@click.argument("market_name")
@click.argument("authfile", type=click.File("r"))
@click.option("--indent", type=int, help="format using INDENT indentation")
@click.option("--raw", is_flag=True, help="display raw response")
@click.option("--count", type=click.IntRange(min=0), default=0, help="display only COUNT items")
@click.option("--age", type=click.IntRange(min=0), default=0, help="display only AGE old entries, in unix timestamp ticks")
def transactions(market_name, authfile, indent, raw, count, age):
    credentials = json.loads(authfile.read())
    private_api = create_private(market_name)
    private_api.setup(credentials)
    data = private_api.get_transactions()
    if data is None:
        raise RuntimeError("could not fetch data")
    if age > 0 and data:
        data = filter_last(data[0]["date"] - age, data)
    if count > 0 and count <= len(data):
        data = data[:count]
    if raw:
        print(json.dumps(data, indent=indent, cls=DecimalEncoder))
    else:
        print(printers.private_transactions(data))


@private.command()
@click.argument("market_name")
@click.argument("authfile", type=click.File("r"))
@click.option("--indent", type=int, help="format using INDENT indentation")
@click.option("--raw", is_flag=True, help="display raw response")
def funds(market_name, authfile, indent, raw):
    credentials = json.loads(authfile.read())
    private_api = create_private(market_name)
    private_api.setup(credentials)
    data = private_api.get_funds()
    if raw:
        print(json.dumps(data, indent=indent, cls=DecimalEncoder))
    else:
        print(printers.private_funds(data))


@private.command()
@click.argument("market_name")
@click.argument("authfile", type=click.File("r"))
@click.option("--indent", type=int, help="format using INDENT indentation")
@click.option("--raw", is_flag=True, help="display raw response")
def offers(market_name, authfile, indent, raw):
    credentials = json.loads(authfile.read())
    market_api = create_private(market_name)
    market_api.setup(credentials)
    data = market_api.get_offers()
    if raw:
        print(json.dumps(data, indent=indent, cls=DecimalEncoder))
    else:
        print(printers.private_offers(data))


@private.command()
@click.argument("market_name")
@click.argument("cpair", callback=split_cpair)
@click.argument("offer_type", type=click.Choice(["bid", "ask"]))
@click.argument("price", type=decimal.Decimal)
@click.argument("amount", type=decimal.Decimal)
@click.argument("authfile", type=click.File("r"))
def make(market_name, cpair, offer_type, price, amount, authfile):
    credentials = json.loads(authfile.read())
    private_api = create_private(market_name)
    private_api.setup(credentials)
    if offer_type == "bid":
        data = private_api.make_bid(cpair, price, amount)
    else:
        data = private_api.make_ask(cpair, price, amount)
    print(data)


@private.command()
@click.argument("market_name")
@click.argument("oid", type=click.IntRange(min=0))
@click.argument("authfile", type=click.File("r"))
def cancel(market_name, oid, authfile):
    credentials = json.loads(authfile.read())
    private_api = create_private(market_name)
    private_api.setup(credentials)
    data = private_api.cancel(oid)
    if data:
        print("offer deleted")
    else:
        print("offer could not be deleted")
