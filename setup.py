import setuptools

setuptools.setup(
    name="btctrader",
    version="0.2",
    description="btctrader",
    url="https://gitlab.com/omicronns.main/btctrader.git",
    author=["Konrad Adasiewicz"],
    author_email=["omicronns@gmail.com"],
    license="",
    packages=["btctrader"],
    install_requires=["requests", "ecdsa", "monotable", "click"],
    entry_points={
        "console_scripts": [
            "btctrader=btctrader.btctrader:btctrader",
        ],
    })
